'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'BaseController.index')

// authentication
Route.group(() => {
  Route.post('register', 'AuthController.register').validator('StoreUser')
  Route.post('login', 'AuthController.login').validator('LoginUser')
  Route.get('me', 'AuthController.getInfo').middleware('auth')
}).prefix('auth')

// posts
Route.group(() => {
  Route.post('/', 'PostController.store').validator('BasePost')
  Route.get('/:slug', 'PostController.show')
  Route.get('/', 'PostController.index')
  Route.put('/:id', 'PostController.update').validator('BasePost')
  Route.delete('/:id', 'PostController.destroy')
}).prefix('posts').middleware('auth')
