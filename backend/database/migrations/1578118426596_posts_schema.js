'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PostsSchema extends Schema {
  up () {
    this.table('posts', (table) => {
      table.string('title')
      table.string('slug')
      table.string('image')
      table.string('content')
      table.string('userId')
      table.timestamps()
    })
  }

  down () {
    this.drop('posts')
  }
}

module.exports = PostsSchema
