'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
const Logger = use('Logger')

class UserSeeder {
  async run () {
    Logger.info('seeding users...')
    await User.query().delete()
    await Factory
      .model('App/Models/User')
      .createMany(10)
    const users = await User.query().fetch()
    Logger.info('User seed data: %o', users.toJSON())
  }
}

module.exports = UserSeeder
