'use strict'

/*
|--------------------------------------------------------------------------
| PostSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Post = use('App/Models/Post')
const Logger = use('Logger')

class PostSeeder {
  async run () {
    Logger.info('seeding posts...')
    await Post.query().delete()
    await Factory
      .model('App/Models/Post')
      .createMany(1000)
    const posts = await Post.query().fetch()
    Logger.info('Post seed data: %o', posts.toJSON())
  }
}

module.exports = PostSeeder
