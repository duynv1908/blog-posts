'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/User', (faker, i, data) => {
  const username = data.username || faker.username()
  const email = data.email || `${username}@email.com`
  const password = data.password || 'Aa1234@'
  return {
    username,
    email,
    password
  }
})

Factory.blueprint('App/Models/Post', (faker, i, data) => {
  const title = data.title || faker.sentence({ words: 5 })
  const image = data.image || '//via.placeholder.com/300'
  const content = data.content || faker.paragraph({ sentences: 5 })
  const userId = data.userId || 'root'
  return {
    title,
    image,
    content,
    userId
  }
})
