'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  /**
   * Each model has a boot phase
   */
  static boot () {
    super.boot()

    /** Custom _id as String instead of ObjectID */
    this.addTrait('AutoId')

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', 'UserHook.hashPassword')
  }

  // static get foreignKey () {
  //   return 'userId'
  // }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  // tokens () {
  //   return this.hasMany('App/Models/Token')
  // }

  // static get table () {
  //   return 'users'
  // }

  static get collection () {
    return 'users'
  }

  /**
   * Type of mongodb.ObjectID The objectId fields will be
   * converted to mongodb.ObjectID before save to db.
   *
   * default return ['_id']
   */
  static get objectIDs () {
    return []
  }

  static get hidden () {
    return ['password']
  }

  static get createdAtColumn () {
    return 'createdAt'
  }

  static get updatedAtColumn () {
    return 'updatedAt'
  }
}

module.exports = User
