'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Post extends Model {
  /**
   * Each model has a boot phase
   */
  static boot () {
    super.boot()

    /** Custom _id as String instead of ObjectID */
    this.addTrait('AutoId')
    this.addTrait('Slugify', {
      fields: { slug: 'title', field: 'slug' },
      disableUpdates: false
    })
    this.addGlobalScope(builder => {
      builder.orderBy('createdAt', 'desc')
    })
  }

  // static get table () {
  //   return 'posts'
  // }

  static get collection () {
    return 'posts'
  }

  /**
   * Type of mongodb.ObjectID The objectId fields will be
   * converted to mongodb.ObjectID before save to db.
   *
   * default return ['_id']
   */
  static get objectIDs () {
    return []
  }

  static get createdAtColumn () {
    return 'createdAt'
  }

  static get updatedAtColumn () {
    return 'updatedAt'
  }

  static scopeBelongToUser (query, id) {
    return query.where('userId', id)
  }
}

module.exports = Post
