'use strict'

const slug = use('slug')

class Slugify {
  register (Model, customOptions = {}) {
    const defaultOptions = { lower: true, fields: {} }
    const options = Object.assign(defaultOptions, customOptions)
    const fields = options.fields
    const field = fields.field
    const source = fields.slug

    Model.queryMacro('whereSlug', (value) => {
      return Model.where(field, value)
    })

    if (fields) {
      Model.addHook('beforeCreate', async (modelInstance) => {
        modelInstance[field] = await this._createUniqueSlug(modelInstance, modelInstance[source], options)
      })

      if (!options.disableUpdates) {
        Model.addHook('beforeUpdate', async (modelInstance) => {
          modelInstance[field] = await this._createUniqueSlug(modelInstance, modelInstance[source], options)
        })
      }
    }
  }

  async _createUniqueSlug (model, source, options) {
    const generatedSlug = slug(source, options)

    return `${generatedSlug}-${model.primaryKeyValue}`
  }
}

module.exports = Slugify
