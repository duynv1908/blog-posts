'use strict'

class UserRepo {
  static get inject () {
    return ['UserModel']
  }

  constructor (User) {
    this.User = User
  }

  /**
   * register a new user
   *
   * @param {Object} payload data to create
   *
   * @return {User}
   */
  register (payload) {
    return this.User.create({ ...payload })
  }
}

module.exports = UserRepo
