'use strict'

const { test, trait, beforeEach } = use('Test/Suite')('Post')
const Factory = use('Factory')
const User = use('UserModel')
const Post = use('PostModel')
let userData = {}
const postData = {
  title: 'title',
  content: 'content'
}

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await User.query().delete()
  await Post.query().delete()
  userData = await Factory.model('App/Models/User').create()
})

test('make sure user can create a post', async ({ assert, client }) => {
  const response = await client
    .post('posts')
    .loginVia(userData, 'jwt')
    .send(postData)
    .end()

  response.assertStatus(201)
  response.assertJSONSubset(postData)
})

test('make sure user can get a post by slug', async ({ assert, client }) => {
  const post = await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })
  const response = await client
    .get(`posts/${post.slug}`)
    .loginVia(userData, 'jwt')
    .end()

  response.assertStatus(200)
  response.assertJSONSubset(postData)
})

test('make sure user can update a post by id', async ({ assert, client }) => {
  const post = await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })

  const newPostData = {
    title: 'new title',
    content: 'new content'
  }
  const response = await client
    .put(`posts/${post.primaryKeyValue}`)
    .loginVia(userData, 'jwt')
    .send(newPostData)
    .end()

  response.assertStatus(200)
  response.assertJSONSubset(newPostData)
})

test('make sure user can delete a post by id', async ({ assert, client }) => {
  const post = await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })

  const response = await client
    .delete(`posts/${post.primaryKeyValue}`)
    .loginVia(userData, 'jwt')
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({ id: post.primaryKeyValue })
})

test('make sure user can get own posts', async ({ assert, client }) => {
  // create 2 post of the user
  await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })
  await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })

  // 1 post of other user
  await Factory.model('App/Models/Post').create({
    ...postData,
    userId: 'otheruserid'
  })

  const response = await client
    .get('posts')
    .loginVia(userData, 'jwt')
    .end()

  const data = response.body
  response.assertStatus(200)
  assert.equal(data.total, 2)
})

test('make sure client can get posts', async ({ assert, client }) => {
  // create 2 post of the user
  await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })
  await Factory.model('App/Models/Post').create({
    ...postData,
    userId: userData.primaryKeyValue
  })

  // 1 post of other user
  await Factory.model('App/Models/Post').create({
    ...postData,
    userId: 'otheruserid'
  })

  const response = await client
    .get('/')
    .end()

  const data = response.body
  response.assertStatus(200)
  assert.equal(data.total, 3)
})
