export const state = () => ({})

export const getters = {}

export const mutations = {}

export const actions = {
  async register({ commit }, payload) {
    await this.$axios.$post('/api/auth/register', payload)
  }
}
