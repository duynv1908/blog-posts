export const state = () => ({
  posts: {}
})

export const getters = {
  posts(state) {
    return state.posts
  }
}

export const mutations = {
  setPosts(state, payload) {
    state.posts = payload
  }
}

export const actions = {
  async fetchData({ commit }, { page, limit }) {
    const data = await this.$axios.$get('/api/', {
      params: {
        limit: limit === -1 ? 999999 : limit,
        page
      }
    })
    commit('setPosts', data)
  },

  async nuxtServerInit({ dispatch }) {
    // await dispatch('area/fetchList')
  },

  async nuxtClientInit({ dispatch }) {
    // await dispatch('area/fetchList')
  }
}
