module.exports = {
  apps: [
    {
      name: 'hogugu-webtool',
      script: 'npm',
      args: 'start',
      env: {
        NODE_ENV: 'development',
        API_URL: 'http://hogugu.scrum-dev.com',
        HOST: '0.0.0.0',
        PORT: '3030',
        BASE: '/webtool/'
      },
      env_production: {
        NODE_ENV: 'production',
        API_URL: 'http://sv-test-001.hogugu.com',
        HOST: '0.0.0.0',
        PORT: '3030',
        BASE: '/webtool/'
      }
    }
  ],
  deploy: {
    develop: {
      user: 'ubuntu',
      host: 'hogugu.scrum-dev.com',
      key: '~/.ssh/config',
      ref: 'origin/develop',
      repo: 'git@bitbucket.org:hidesignsJP/hogugu-webtool.git',
      path: '/home/ubuntu/hogugu-webtool',
      'pre-setup': 'rm -rf ~/hogugu-webtool/source',
      // 'post-setup': 'cp ~/ENV/webtool_env ~/hogugu-webtool/source/.env',
      'post-deploy':
        'npm install && npm run build && pm2 startOrRestart ecosystem.config.js',
      env: {
        NODE_ENV: 'development',
        API_URL: 'http://hogugu.scrum-dev.com',
        HOST: '0.0.0.0',
        PORT: '3030',
        BASE: '/webtool/'
      }
    },
    production: {
      user: 'ubuntu',
      host: 'sv-test-001.hogugu.com',
      key: '~/.ssh/config',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:hidesignsJP/hogugu-webtool.git',
      path: '/home/ubuntu/hogugu-webtool',
      'pre-setup': 'rm -rf ~/hogugu-webtool/source',
      // 'post-setup': 'cp ~/ENV/webtool_env ~/hogugu-webtool/source/.env',
      'post-deploy':
        'npm install && npm run build && pm2 startOrRestart ecosystem.config.js'
    }
  }
}
