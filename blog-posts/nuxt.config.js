import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
import webpack from 'webpack'
import pkg from './package'

const apiUrl = process.env.API_URL || 'http://localhost:3000'
const port = process.env.PORT || '3030'
const host = process.env.HOST || '0.0.0.0'
const base = process.env.BASE || '/'

export default {
  server: {
    port,
    host
  },

  router: {
    base
  },

  mode: 'spa',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: `${base}favicon.ico` },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: ['~/assets/style/app.styl'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/components/global',
    '@/plugins/vuetify',
    '@/plugins/veeValidate',
    '@/plugins/vueMoment',
    { src: '@/plugins/vueJsonPretty', ssr: false },
    { src: '@/plugins/nuxt-client-init', ssr: false }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    '@nuxtjs/toast'
  ],

  watchers: {
    webpack: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },

  /**
   * Toast module configuration
   */
  toast: {
    position: 'top-right',
    duration: 5 * 1000,
    keepOnHover: true,
    register: [
      {
        name: 'my-error',
        message: 'Oops...Something went wrong',
        options: {
          type: 'error'
        }
      }
    ]
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    proxy: true,
    credentials: false
  },

  proxy: {
    '/api/': { target: apiUrl, pathRewrite: { '^/api/': '' } }
  },

  auth: {
    cookie: false,
    redirect: {
      login: '/auth/login',
      logout: '/auth/login',
      callback: '/auth/login',
      home: '/management'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/auth/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: false,
          user: {
            url: '/api/auth/me',
            method: 'get',
            propertyName: false
          }
        },
        tokenType: 'Bearer'
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    // publicPath: base,
    transpile: ['vuetify/lib'],
    plugins: [
      new VuetifyLoaderPlugin(),
      new webpack.ProvidePlugin({
        _: 'lodash'
      })
    ],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
